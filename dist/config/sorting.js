"use strict";

Object.defineProperty(exports, "__esModule", {
            value: true
});
var desc = 1;
var asc = -1;

var sortconfig = [{ param: "nbTypos", direction: desc }, { param: "firstMatchedWord", direction: desc }, { param: "proximityDistance", direction: desc }, { param: "userScore", direction: asc }, { param: "nbExactWords", direction: desc }, { param: "words", direction: desc }, { param: "filters", direction: desc }];
exports.default = sortconfig;
module.exports = exports["default"];