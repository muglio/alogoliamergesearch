"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var algoliasearch = require('algoliasearch');
var sortConfig = require("../config/sorting.js");

var QueryBuilder = exports.QueryBuilder = function QueryBuilder(indexes, query) {
  //handle string case
  if (typeof indexes === 'string') {
    return [{ indexName: indexes,
      query: query,
      params: {
        getRankingInfo: false
      }
    }];
  }

  return indexes.map(function (i) {
    return {
      indexName: i,
      query: query,
      params: {
        getRankingInfo: indexes.length > 1
      }
    };
  });
};

var _CleanResults = exports._CleanResults = function _CleanResults(hit) {
  delete hit._rankingInfo;
  return hit;
};

var _MergeResults = exports._MergeResults = function _MergeResults(result) {
  return result.hits.map(function (hit) {
    hit.index = result.index;
    return hit;
  });
};

var _SearchSort = exports._SearchSort = function _SearchSort(hita, hitb) {
  for (var i = 0, len = sortConfig.length; i < len; i++) {
    var measure = sortConfig[i];
    if (hita._rankingInfo[measure.param] < hitb._rankingInfo[measure.param]) return -1 * measure.direction;
    if (hita._rankingInfo[measure.param] > hitb._rankingInfo[measure.param]) return 1 * measure.direction;
  }
  return 0;
};

var _Validate = exports._Validate = function _Validate(options) {
  //todo: put this somewhere half intelligent
  var minReq = ['indices', 'query', 'results'];
  minReq.map(function (param) {
    if (typeof options === 'undefined' || options == null) throw Error('options is null');
    if (typeof options[param] === 'undefined') throw Error('options ' + param + ' is required,');
    return null;
  });
};

var Search = exports.Search = function Search(options) {
  //intent: blow up on validation error
  _Validate(options);
  if (typeof options.client == "undefined") {
    options.client = algoliasearch("REGR6S7KAU", "6b563c69f83300838242aad32d501ddd");
  }

  var queries = QueryBuilder(options.indices, options.query);

  options.client.search(queries, function (err, content) {
    var _ref;

    if (err) {
      options.error(err);
      return;
    }
    var hits = (_ref = []).concat.apply(_ref, _toConsumableArray(content.results.map(_MergeResults)));
    if (options.indices.length > 1) {
      hits = hits.sort(_SearchSort).map(_CleanResults);
    }
    options.results(hits);
  });
};