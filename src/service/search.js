import { ALGOLIA_SEARCH_KEY, ALGOLIA_APP_KEY } from "babel-dotenv"
let algoliasearch = require('algoliasearch');
let sortConfig = require("../config/sorting.js");

export const QueryBuilder = (indexes, query) => {
  //handle string case
  if (typeof(indexes) ===  'string') {
    return [{indexName: indexes,
      query: query,
      params: {
        getRankingInfo: false
      }
    }];
  }

  return indexes.map((i) => {return {
    indexName: i,
    query: query,
    params: {
      getRankingInfo: indexes.length > 1
    }
  }});
}

export const _CleanResults = (hit) => {
  delete hit._rankingInfo;
  return hit;
}

export const _MergeResults = (result) =>
  result.hits.map((hit) => {
    hit.index = result.index;
    return hit;
});

export const _SearchSort = (hita, hitb) => {
  for (var i = 0, len = sortConfig.length; i < len; i++) {
    var measure = sortConfig[i];
    if (hita._rankingInfo[measure.param] < hitb._rankingInfo[measure.param]) return -1 * measure.direction;
    if (hita._rankingInfo[measure.param] > hitb._rankingInfo[measure.param]) return 1  * measure.direction;
  }
  return 0;
}

export const _Validate = (options) => {
  //todo: put this somewhere half intelligent
  let minReq = ['indices','query','results'];
  minReq.map((param)=>{
    if (typeof(options) === 'undefined' || options == null) throw Error('options is null');
    if (typeof(options[param]) === 'undefined') throw Error('options ' + param + ' is required,');
    return null;
  });
}

export const Search = (options) => {
  //intent: blow up on validation error
  _Validate(options);
  if (typeof options.client == "undefined") {
    options.client = algoliasearch(ALGOLIA_APP_KEY, ALGOLIA_SEARCH_KEY);
  }

  let queries = QueryBuilder(options.indices,options.query);

  options.client.search(queries,(err,content) => {
    if (err) {
      options.error(err);
      return;
    }
    var hits = [].concat(...content.results.map(_MergeResults));
    if (options.indices.length > 1) {
     hits = hits.sort(_SearchSort)
                .map(_CleanResults);
    }
    options.results(hits);
  });
}
