var desc = 1;
var asc = -1;

let sortconfig = [
            {param: "nbTypos", direction: desc},
            {param: "firstMatchedWord", direction: desc},
            {param: "proximityDistance", direction: desc},
            {param: "userScore", direction: asc},
            {param: "nbExactWords", direction: desc},
            {param: "words", direction: desc},
            {param: "filters", direction: desc}
          ];
export default sortconfig;
