Searches multiple indexes and returns results in a merged result set.

## Installation ##

npm install https://muglio@bitbucket.org/muglio/alogoliamergesearch.git --save


## Dependencies ##

Add the following keys to your .env file.

ALGOLIA_APP_KEY=*APPLICATION ID OBTAINED FROM ALGOLIA DASHBOARD*

ALGOLIA_SEARCH_KEY=*API KEY OBTAIN FROM ALGOLIA DASHBOARD*

## Usage ##


```
#!javascript

let algoliaClient = require('alogoliamergesearch');

algoliaClient.SearchAPI.Search({
                                    indices: ["learning_curve"],
                                    query: "Circulation",
                                    error: (error) => console.error(error),
                                    results: (results) => {
                                        console.log(results);
                                    }
                                  });
```