import { NODE_ENV } from "babel-dotenv"
let queryString = require('query-string');
let WH = require('./fakeindices/wh-comma');
let WH2 = require('./fakeindices/wh2-comma');



const algoliasearch = {
  search: (queries, callback) => {
      var o = queries.map(q => {
          var hits = eval(q.indexName)().map((hit) => {
            if (queries.length < 2) {
              delete hit._rankingInfo;
            }
            return hit;
          });
          q.params.query = q.query;
          return {
            hits: hits,
            nbHits: hits.length,
            hitsPerPage: 20,
            page: 0,
            nbPages: 1,
            processingTimeMS: 1,
            exhaustiveNbHits: true,
            query: q.query,
            params: queryString.stringify(q.params),
            index: q.indexName
          }
        });
        var r = {results: o};
        callback(null,r);
  }
};

export default algoliasearch;
