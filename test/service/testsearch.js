var assert = require('assert');
var searchsvc = require('../../src/service/search');
let mockclient = require('./mockalgoliasearch')




describe('When searching in a single index the results', () => {
  it('should not return page rank information', () => {
      searchsvc.Search({
                        indices: ["WH"],
                        query: "comma",
                        error: (error) => console.error(error),
                        results: (results) => {
                          assert.equal(typeof(results[0]._rankingInfo),'undefined');
                        },
                        client: mockclient
                    });
  });
  it('should contain the index name',() => {
    searchsvc.Search({
                    indices: ["WH"],
                    query: "comma",
                    error: (error) => console.error(error),
                    results: (results) => {
                        assert.equal(results[0].index,'WH');
                    },
                    client: mockclient
                  });
  });
});


describe('When searching in a multiple index the results', () => {
  it('should not return page rank information',() => {
    searchsvc.Search({
                    indices: ["WH","WH2"],
                    query: "comma",
                    error: (error) => console.error(error),
                    results: (results) => {
                        assert.equal(typeof(results[0]._rankingInfo),'undefined');
                    },
                    client: mockclient
                  });
  });
  it('should contain the index name',() => {
    searchsvc.Search({
                    indices: ["WH","WH2"],
                    query: "comma",
                    error: (error) => console.error(error),
                    results: (results) => {
                        assert.equal(results[1].index,"WH2");
                    },
                    client: mockclient
                  });
  });
  it('should be properly ordered by rank', () => {
    searchsvc.Search({
                    indices: ["WH","WH2"],
                    query: "comma",
                    error: (error) => console.error(error),
                    results: (results) => {
                        var expectedOrder = [1,5,2,6,4,8,7,3];
                        for (var i = 1; i < expectedOrder.length;i++) {
                          assert.equal(results[i].objectID,expectedOrder[i]);
                        }
                    },
                    client: mockclient
                  });
  })
});
