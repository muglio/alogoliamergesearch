let WH2 = function mock() {
  return [{ 'url': 'dlap://level1_W0170',
   title: 'comma splice',
   objectID: '5',
   _rankingInfo:
    { nbTypos: 0,
      firstMatchedWord: 0,
      proximityDistance: 0,
      userScore: 173,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
},
 { 'url': 'dlap://level1_W0275',
   title: 'direct address, comma with',
   objectID: '6',
   _rankingInfo:
    { nbTypos: 0,
      firstMatchedWord: 2,
      proximityDistance: 0,
      userScore: 279,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
},
{ 'url': 'dlap://level1_W0221',
   title: 'compass points, capitalization of',
   objectID: '7',
   _rankingInfo:
    { nbTypos: 1,
      firstMatchedWord: 0,
      proximityDistance: 0,
      userScore: 187,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
},
{ 'url': 'dlap://level1_W0220',
   title: 'contrasting elements, commas with',
   objectID: '8',
   _rankingInfo:
    { nbTypos: 0,
      firstMatchedWord: 2,
      proximityDistance: 0,
      userScore: 225,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
}
]
};

export default WH2;
