let WH = function mock() {
  return [{ 'url': 'dlap://level1_W0170',
   title: 'comma splice',
   objectID: '1',
   _rankingInfo:
    { nbTypos: 0,
      firstMatchedWord: 0,
      proximityDistance: 0,
      userScore: 173,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
},
 { 'url': 'dlap://level1_W0275',
   title: 'direct address, comma with',
   objectID: '2',
   _rankingInfo:
    { nbTypos: 0,
      firstMatchedWord: 2,
      proximityDistance: 0,
      userScore: 279,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
},
{ 'url': 'dlap://level1_W0221',
   title: 'commands',
   objectID: '3',
   _rankingInfo:
    { nbTypos: 1,
      firstMatchedWord: 0,
      proximityDistance: 0,
      userScore: 174,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
},
{ 'url': 'dlap://level1_W0220',
   title: 'contrasting elements, commas with',
   objectID: '4',
   _rankingInfo:
    { nbTypos: 0,
      firstMatchedWord: 2,
      proximityDistance: 0,
      userScore: 225,
      geoDistance: 0,
      geoPrecision: 1,
      nbExactWords: 0,
      words: 1,
      filters: 0 }
}
];
}

export default WH;
