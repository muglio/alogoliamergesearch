var assert = require('assert');
var searchsvc = require('../../src/service/search');

//Indexes in here are totally BS they don't need to correspond to a mock

describe('Query Builder - Single Index in Array', () => {
  let queries = searchsvc.QueryBuilder(['wh'],"comma");
  it('should have the first indexName of wh',() => {
   assert.equal(queries[0].indexName,'wh');
  });
  it('should have the first query of comma',() => {
   assert.equal(queries[0].query,'comma');
  });
  it('should not request ranking info',() => {
   assert.equal(queries[0].params.getRankingInfo,false);
  });
});

describe('Query Builder - Single Index as String', () => {
  let queries = searchsvc.QueryBuilder('wh',"comma");
  it('should have the first indexName of wh',() => {
    assert.equal(queries[0].indexName,'wh');
  });
  it('should have the first query of comma',() => {
    assert.equal(queries[0].query,'comma');
  });
  it('should not request ranking info',() => {
    assert.equal(queries[0].params.getRankingInfo,false);
  });
});

describe('Query Builder - Multi Index as String', () => {
  let queries = searchsvc.QueryBuilder(['wh','lc'],"comma");
  it('should have the first indexName of wh',() => {
   assert.equal(queries[0].indexName,'wh');
  });
  it('should have the first query of comma',() => {
   assert.equal(queries[0].query,'comma');
  });
  it('should have the second indexName of lc',() => {
   assert.equal(queries[1].indexName,'lc');
  });
  it('should have the second query of comma',() => {
   assert.equal(queries[1].query,'comma');
  });
  it('should request ranking info',() => {
   assert.equal(queries[0].params.getRankingInfo,true);
   assert.equal(queries[1].params.getRankingInfo,true);
  });
});


//searchsvc.Search(require('./mock-results'),['wh'],"comma");

// describe('DUMMY', function() {
//    it('should return true',function() {
//     assert.equal(1,1);
//    });
// });
